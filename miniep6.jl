# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)
# Add answer to MiniEP6’s part 2.1 exercise

function impares_consecutivos(n)
    # escreva sua solução aqui       #Função Somatório para passar o valor no lugar do n abaixo!
       k=0
       m=0
        for m in 0:n
           k = k + m
           m = m + 1
        end

       #println("O valor de k eh ", k)


       #Procedimento que popula o vetor em números de casas  conforme o valor do  Somatório
       v = Array{Int64}(undef, k)  # Declaração de um vetor de k posições (colunas) com uma linha

       x = 1

        for i in 1:k

           v[i] = x  # x é o valor atribuído a cada índice do vetor
           
           i = i + 1 # i é o valor atribuído ao índice do vetor a ser percorrido

           x = x + 2

        end


       #Procedimento que percorre o vetor imprimindo seus valores
        #for j in 1:k 

          #println(v[j])
        
          #j = j + 1 
                   
        #end


       #Procedimento que retorna o primeiro ímpar exigido
       return v[(k+1)-n]
       



      #Só mais uma maneira de exemplificar a impressão dos valores do vetor em Julia
       #for el in v
       #println("!", el)
       #end



       #Função Somatório para passar o valor no lugar do n abaixo!
       k=0
       m=0
        for m in 0:n
           k = k + m
           m = m + 1
        end

       #println("O valor de k eh ", k)


       #Procedimento que popula o vetor em números de casas  conforme o valor do  Somatório
       v = Array{Int64}(undef, k)  # Declaração de um vetor de k posições (colunas) com uma linha

       x = 1

        for i in 1:k

           v[i] = x  # x é o valor atribuído a cada índice do vetor
           
           i = i + 1 # i é o valor atribuído ao índice do vetor a ser percorrido

           x = x + 2

        end


       #Procedimento que percorre o vetor imprimindo seus valores
        #for j in 1:k 

          #println(v[j])
        
          #j = j + 1 
                   
        #end


       #Procedimento que retorna o primeiro ímpar exigido
       return v[(k+1)-n]
       



      #Só mais uma maneira de exemplificar a impressão dos valores do vetor em Julia
       #for el in v
       #println("!", el)
       #end

end





# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)
# Add answer to MiniEP6’s part 2.2 exercise.


function imprime_impares_consecutivos(m)
# escreva sua solução aqui


       #Função Somatório para passar o valor no lugar do n abaixo!
       k=0
       y=0
        for y in 0:m
           k = k + y
           y = y + 1
        end

       #println("O valor de k eh ", k)


       #Procedimento que popula o vetor em números de casas  conforme o valor do  Somatório
       v = Array{Int64}(undef, k)  # Declaração de um vetor de k posições (colunas) com uma linha

       x = 1

        for i in 1:k

           v[i] = x  # x é o valor atribuído a cada índice do vetor
           
           i = i + 1 # i é o valor atribuído ao índice do vetor a ser percorrido

           x = x + 2

        end


       #Procedimento que percorre o vetor imprimindo seus valores
        
       print(m, " ", m^3)

       j = (k+1)-m

        for j in j:k 

          print(" ", v[j])
        
          j = j + 1 
                   
        end


       #Procedimento que percorre o vetor a partir do respectivo primeiro ímpar até o final
       #return v[(k+1)-m]
       



      #Só mais uma maneira de exemplificar a impressão dos valores do vetor em Julia
       #for el in v
       #println("!", el)
       #end

end





function mostra_n(n)
    # escreva sua solução aqui


  m=1

    while m <= n
    imprime_impares_consecutivos(m)
    println()
    m = m + 1
    end

end





# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()